Google+ API Information

=======================
## Theory

### Posts
Because of the design of the Google+, there is no messages at all. The only way to send someone the information, article, image, video, etc. is to post
it for someone. The variants are: public (post can be accessed by anyone in Google+) or protected (post can be accessed by chosen people and "circles").
Google+ api allows you to access only to public posts.

### Circles
Circles in G+ replaces Groups and types of subscription. Unfortunately, Google+ API does not allow us to use this feature.

### Comments
Comments can be all comments of all accessable posts can be retrieved. They has link to post where they were added.

### People
People can be searched, get, listed by activity and by another user.

### Authentication
Google+ API uses OAuth 2.0 authentication,so you have to ways to interact with server:
 * via REST-requests
 * through Google client library
In both ways you should get Client ID and API key. They are generated in Google Developer's Console.

### Google Developer's Console
To generate Client ID you should follow this steps:  
[https://developers.google.com/identity/sign-in/web/devconsole-project](https://developers.google.com/identity/sign-in/web/devconsole-project)

To generate API key you should:
 1. All steps of Client ID generation should be done.
 2. In Credentials tab press "Create Credentials" button
 3. Select API key option
 4. Select Browser key and set up your API key

## Access via JavaScript

**Hope you already have Client ID and API key. If not got to the "Google Developer's Console" section.**

### Integration
To integrate your web app with Google+ follow this steps:
 1. Put `<meta>` tag in `<head>` container:  
 `<meta name="google-signin-client_id" content="YOUR_CLIENT_ID">`  
 Replace `YOUR_CLIENT_ID` with your app's Client ID(including ".apps.googleusercontent.com")
 2. Add `<script>`:  
 `<script src="https://apis.google.com/js/platform.js" async defer></script>`
 3. Initialize OAuth 2.0 with GAPI:  
 ```javascript
   gapi.load(‘auth2’, function(){
     gapi.auth2.init();
   });
 ```
 4. Add Sign-In button to your UI:  
 `<div class="g-signin2" data-onsuccess="YOUR_ON_SUCCESS_FUNCTION"></div>`  
 GAPI will automaticly fin and render it(if you want custom rendering see [this](https://developers.google.com/identity/sign-in/web/build-button))
 5. Write your function to operate with user data("YOUR_ON_SUCCESS_FUNCTION")(below is function declaration)
 ```javascript 
	function YOUR_ON_SUCCESS_FUNCTION(googleUser){
		// from googleUser you can fetch userId
		// necessary thing to manipulate with user's account
	}
 ```
 6. To sign out the user you can use this code:
 ```javascript	
	var auth2 = gapi.auth2.getAuthInstance();
	//function passed to "then" will be invoked after sign out 
    auth2.signOut().then(function () { 
      console.log('User signed out.');
    });
 ```
 
### Fetching public posts
As I told in Theory.Posts section Google+ API allows access only to public posts.
Here is code snippet to get all public posts of signed user(or replace *me* with *userID*).
```javascript
    var request = gapi.client.plus.activities.list({
		'userId' : 'me',
		'collection' : 'public'
	});

	request.execute(function(resp) {
		var numItems = resp.items.length;
		for (var i = 0; i < numItems; i++) {
			//here you operate with data
		}
	});
```
[Here](https://developers.google.com/+/web/api/rest/latest/activities) you can see documentation for activities(posts, articles) resource bundle.

### Getting subscribed people
All information about people you subscribed you can get using this code:
```javascript
    var request = gapi.client.plus.people.list({
		'userId' : 'me',
		'collection' : 'visible'
	});

	request.execute(function(resp) {
		var numItems = resp.items.length;
		for (var i = 0; i < numItems; i++) {
			// manipulate with result
		}
	});
```
[Here](https://developers.google.com/+/web/api/rest/latest/people) you can see documentation for people resource bundle.